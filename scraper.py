from lxml import html
import requests
import timeit
import queue
import threading
import time
from flask import Flask, render_template, request, jsonify, redirect, url_for, Response
import os
from config import tlds
import datetime
import json
import xlwt

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
# socketio = SocketIO(app)
keyProcessed = 0
exitFlag = 0

class myThread(threading.Thread):
    def __init__(self, threadID, name, session_name, q, lock, save_file_name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
        self.lock = lock
        self.save_file_name = save_file_name
        self.session_name = session_name
    
    def run(self):
        print ("Starting " + self.name)
        self.lookup( self.q, self.lock)
        print ("Exiting " + self.name)

    def lookup(self, q, lock):
        while not exitFlag:
            lock.acquire()
            if not q.empty():
                global keyProcessed
                self.query = q.get()
                self.payload = {'q':self.query, 'count':30}
                self.urlReq = 'https://www.bing.com/search'
                try:
                    self.page = requests.get(self.urlReq, params=self.payload)
                except:
                    pass
                self.tree = html.fromstring(self.page.content)    
                lock.release()
                keyProcessed+=1
                print('Processing keyword ke : {}'.format(keyProcessed))
                update_progress(self.session_name, keyProcessed)
                self.file = open('result/'+ self.save_file_name, 'a')
                try:
                    self.subdomains = ['tumblr', 'blogspot', 'wordpress', 'weebly', 'wix', 'squarespace']
                    # self.dList = ['com', 'co', 'org', 'ac', 'id', 'net', 'web', 'asia']
                    self.results = self.tree.xpath('//*[@id="b_results"]/li/h2/a')
                    self.count = 0
                    self.domains = []
                    self.strPoss = []
                    for i in self.results:
                        self.url = str(i.get('href'))
                        # print(self.url)
                        self.splitHttp = self.url.split('//')
                        self.splitSub = self.splitHttp[1].split('/')
                        self.domaincom = self.splitSub[0]
                        try:
                            self.domaincom = self.domaincom.strip('www.')
                        except:
                            pass
                        self.domain = self.domaincom.split('.')
                        # print(self.domain)
                        self.domainName = self.domain[0]
                        if self.domain[1].upper() in tlds and len(self.domain) < 4:
                            if str(self.query) in str(self.domainName):
                                self.strPos = getPos(self.domainName, self.query)
                                if self.domaincom not in self.domains:
                                    self.count+=1
                                    self.domains.append(self.domaincom)
                                    self.strPoss.append(self.strPos) 
    
                        elif self.domain[1] in self.subdomains:
                            if str(self.query) in str(self.domainName):
                                self.strPos = getPos(self.domainName, self.query)
                                if self.domaincom not in self.domains:
                                    self.count+=1
                                    self.domains.append(self.domaincom)
                                    self.strPoss.append(self.strPos)
                        else:
                            pass
                            # print(self.domaincom)
                        
                    for i in range(self.count):
                        try:
                            self.striped_domain = self.domains[i].strip('.www')
                        except:
                            pass
                        self.file.write(str(self.query) + ' ' + str(self.query) + '.com ' + str(self.striped_domain) + ' ' + str(self.count) + ' ' + str(self.strPoss[i]) + '\n' )
                    self.file.close()
                except Exception as e:
                    print(e)
                    print("Failed to load")
        
            else:
                lock.release()

def getPos(str1, str2):
    mid = len(str1)/2
    index = str1.index(str2)
    len1 = len(str1)
    len2 = len(str2)
    if index == 0 and len1 != len2:
        # print 'diawal'
        return 'diawal'
    elif index == 0 and len1 == len2:
        return 'exact'
    else:
        return 'other'
        
def readFile(fileName, session_name):
    queueLock = threading.Lock()
    workQueue = queue.Queue(100000)
    save_file_name = session_name + ' ' + datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S') + '.xls'
    # f = open(save_file_name, 'w')
    # f.close()
    add_progress({
        'name':session_name,
        'progress':0,
        'total': sum(1 for line in open(fileName))
    })

    file = open(fileName, 'r')
    count = 0
    threads = []
    
    # run thread
    threadCount = 20
    for i in range(threadCount):
        thread = myThread(i, "Thread-{}".format(i), session_name, workQueue, queueLock, save_file_name)
        thread.start()
        threads.append(thread)

    # fill queue
    queueLock.acquire()
    
    for i in file:
        # if count == 10:
        #     break
        query = i.rstrip()
        workQueue.put(query)
        count+=1
    queueLock.release()
    
    # wait queue to empty
    while not workQueue.empty():
        pass

    # notify thread to exit
    global exitFlag
    exitFlag = 1
    
    for t in threads:
        t.join()

    # convert file to excel
    book = xlwt.Workbook(encoding="utf-8")
    sheet1 = book.add_sheet("Sheet 1")

    file = open('result/'+save_file_name, 'r')
    index = 0
    for i in file:
        rd = i.strip('\n').split(' ')
        # print(rd)
        sheet1.write(index, 0, rd[0])
        sheet1.write(index, 1, rd[1])
        sheet1.write(index, 2, rd[2])
        sheet1.write(index, 3, rd[3])
        sheet1.write(index, 4, rd[4])
        index+=1

    file.close()
    book.save('result/'+save_file_name)
    print('Total keywords :', count)
 

def startScrape(session_name):
    global exitFlag, keyProcessed
    exitFlag = 0
    keyProcessed = 0
    timeStart = timeit.default_timer()
    readFile('keyFile.txt', session_name)
    timeEnd = timeit.default_timer()
    print('apps run in : ', timeEnd-timeStart)
    print('end')

@app.route('/')
def static_page():
    return render_template('index.html')

@app.route('/progress')
def progress_page():
    return render_template('progress.html')

@app.route('/scrap', methods=['POST'])
def scrap():
    dirname = os.path.split(os.path.abspath(__file__))
    f = request.files['file']
    f.save('keyFile.txt')
    startScrape(f.filename)
    return redirect(url_for('progress_page'))
    
    # return 'file uploaded successfully'

@app.route('/get_progress')
def get_status():
    progress = get_progress()
    print(progress)
    return jsonify(result=progress)
    
# @socketio.on('my event')
# def handle_my_custom_event(json):
#     emit('message', {'data': 'Connected'})
#     print('received json: ' + str(json))


# @socketio.on('message')
# def handle_message(msg):
#     print(msg)


def add_progress(data):
    file = open('process.json', 'r')
    cache = json.load(file)
    file.close()
    if len(cache) > 10:
        cache = []
    cache.append(data)
    file = open( 'process.json', 'w')
    file.write(json.dumps(cache))
    file.close()

def update_progress(name, progress):
    file = open('process.json', 'r')
    cache = json.load(file)
    file.close()
    for i in cache:
        if i['name'] == name: 
            i['progress'] = progress
            break

    file = open( 'process.json', 'w')
    file.write(json.dumps(cache))
    file.close()

def get_progress():
    file = open('process.json', 'r')
    cache = json.load(file)
    file.close()
    return cache

if __name__ == '__main__':
    app.run()
    # socketio.run(app)
