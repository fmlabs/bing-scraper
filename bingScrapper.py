import time
import timeit
import queue
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from flask import Flask, render_template

import threading

app = Flask(__name__)

exitFlag = 0

class myThread (threading.Thread):
    def __init__(self, threadID, name, q, lock):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
        self.lock = lock
        
    def run(self):
        print ("Starting " + self.name)
        self.driver = self.init_driver()
        self.driver.get("https://www.bing.com")
        self.lookup( self.driver, self.q, self.lock)
        self.driver.quit()
        print ("Exiting " + self.name)

    def init_driver(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        self.driver = webdriver.Chrome('/Users/fmlabs/Documents/Work/python/scraping/chromedriver/mac', chrome_options=self.options)
        # self.driver = webdriver.PhantomJS()
        self.driver.wait = WebDriverWait(self.driver, 10)
        return self.driver

    def lookup(self, driver, q, lock):
        while not exitFlag:
            lock.acquire()
            if not q.empty():
                self.query = q.get()
                lock.release()
                print(self.name + ' processing ' + self.query)
                self.driver = driver
                # print("ready")
                # print(self.query)
                self.file = open('result.txt', 'a')
                try:
                    self.driver.find_element_by_xpath('//*[@id="sb_form_q"]').clear()
                    self.driver.find_element_by_xpath('//*[@id="sb_form_q"]').send_keys(self.query)
                    # time.sleep(1)
                    self.driver.find_element_by_xpath('//*[@id="sb_form_go"]').click()
                    self.driver.wait.until(EC.presence_of_element_located((By.ID, 'b_results')))
                    self.results = self.driver.find_elements_by_xpath('//*[@id="b_results"]/li/h2/a')
                    self.count = 0
                    self.domains = ''
                    for i in self.results:
                        self.url = str(i.get_attribute('href'))
                        self.splitHttp = self.url.split('//')
                        self.splitSub = self.splitHttp[1].split('/')
                        self.domain = self.splitSub[0]
                        # print(self.domain)
                        if str(self.query) in str(self.domain):
                            self.count+=1
                            self.domains += (self.domain + ',') 

                    
                    self.file.write('\n' + self.query + ' ' + str(self.count) + ' ' + self.domains)
                    self.file.close()
                except TimeoutException:
                    print("Gagal cok")
        
            else:
                lock.release()
        

def readFile(fileName):
    queueLock = threading.Lock()
    workQueue = queue.Queue(100000)

    file = open(fileName, 'r')
    count = 0
    threads = []
    
    # run thread
    for i in range(10):
        thread = myThread(i, "Thread-{}".format(i), workQueue, queueLock)
        thread.start()
        threads.append(thread)

    # fill queue
    queueLock.acquire()
    for i in file:
        if count == 100:
            break
        query = i.rstrip()
        workQueue.put(query)
        count+=1
    queueLock.release()
    
    # wait queue to empty
    while not workQueue.empty():
        pass

    # notify thread to exit
    global exitFlag
    exitFlag = 1
    
    for t in threads:
        t.join()

    print('Total keywords :', count)
 
if __name__ == "__main__":
    timeStart = timeit.default_timer()
    readFile('sampleKey.txt')
    timeEnd = timeit.default_timer()
    print('apps run in : ', timeEnd-timeStart)
    print('end')


# @app.route('/')
# def static_page():
#     return render_template('index.html')

# @app.route('/scrap')
# def scrap():
#     print('scrap')
#     return 'wew'

# if __name__ == '__main__':
#     app.run()