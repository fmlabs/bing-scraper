#!/bin/bash
sudo apt-get install git -y
sudo apt-get install nginx -y
cd /opt/ && git clone https://gitlab.com/fmlabs/bing-scraper.git 

/opt/bing-scraper/venv/bin/pip install -r /opt/bing-scraper/requirements.txt

sudo mkdir /var/log/uwsgi

echo "[Unit]
Description=uWSGI instance to serve myproject
After=network.target

[Service]
User=root
Group=www-data
WorkingDirectory=/opt/bing-scraper
Environment="PATH=/opt/bing-scraper/venv/bin"
ExecStart=/opt/bing-scraper/venv/bin/uwsgi --ini scraper.ini

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/scraper.service

sudo systemctl enable scraper
sudo systemctl start scraper

echo "server {
    listen 80;
    server_name server_domain_or_IP;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/opt/bing-scraper/scraper.sock;
    }

    location /result {
                alias /opt/bing-scraper/result/;
                autoindex on;
    }
}" >> /etc/nginx/sites-available/scraper

sudo ln -s /etc/nginx/sites-available/scraper /etc/nginx/sites-enabled

sudo rm /etc/nginx/sites-enabled/default

sudo systemctl restart nginx
sudo ufw delete allow 5000
sudo ufw allow 'Nginx Full'